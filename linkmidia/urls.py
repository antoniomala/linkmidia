# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # Examples:
    # url(r'^$', 'linkmidia.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^$', 'apps.website.views.home', name='home'),
    url(r'^testes/para_homens$', 'apps.website.views.testes_para_homens', name='testes_para_homens'),
    url(r'^testes/para_mulheres$', 'apps.website.views.testes_para_mulheres', name='testes_para_mulheres'),

    
    url(r'^criar_teste/$', 'apps.teste.views.criar_teste', name='criar_teste'),
    url(r'^editar_teste/(?P<id>\d+)/$', 'apps.teste.views.editar_teste', name='editar_teste'),
    url(r'^carregar_imagem_teste/(?P<id>\d+)/$', 'apps.teste.views.carregar_imagem_teste', name='carregar_imagem_teste'),
    url(r'^excluir_imagem_teste/(?P<teste>\d+)/(?P<id>\d+)/$', 'apps.teste.views.excluir_imagem_teste', name='excluir_imagem_teste'),
    url(r'^visualizar_imagem_teste/(?P<teste>\d+)/(?P<id>\d+)/$', 'apps.teste.views.visualizar_imagem_teste', name='visualizar_imagem_teste'),
    
    
    url(r'^ver_teste/(?P<teste>.*)/$', 'apps.website.views.ver_teste', name='ver_teste'),
    url(r'^fazer_teste/(?P<teste>.*)/$', 'apps.website.views.fazer_teste', name='fazer_teste'),


    url(r'^administracao/$', 'apps.website.views.administracao', name='administracao'),
    url(r'^administracao/testes/$', 'apps.website.views.testes', name='testes'),

    url(r'^administracao/usuarios/$', 'apps.website.views.usuarios', name='usuarios'),

    url(r'^admin/', include(admin.site.urls)),

    # habilitando a visualização dentro dos arquivos em MEDIA - não usar em produção
    url(r'^media/(.*)/$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
