# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from apps.teste.models import *
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test

import requests
import urllib
import os

from allauth.socialaccount.models import SocialAccount

from PIL import Image
import random

def home(request):
	testes = Teste.objects.all()
	return render(request, 'home.html', {'testes': testes})

@login_required(login_url='/accounts/login/')
def administracao(request):
	return render(request, 'administracao.html')

@login_required(login_url='/accounts/login/')
def testes(request):
	testes = Teste.objects.all()
	return render(request, 'testes.html', {'testes': testes})

def testes_para_homens(request):
	testes = Teste.objects.exclude(categoria='Feminino')
	return render(request, 'home.html', {'testes': testes})	

def testes_para_mulheres(request):
	testes = Teste.objects.exclude(categoria='Masculino')
	return render(request, 'home.html', {'testes': testes})	

@login_required(login_url='/accounts/login/')
def usuarios(request):
	usuarios = User.objects.all()
	return render(request, 'usuarios.html', {'usuarios': usuarios})

def ver_teste(request, teste):
	t = Teste.objects.get(slug=teste)

	lista = Teste.objects.all()
	bot1 = []
	bot2 = []
	i = 0
	j = 0
	if len(lista) > 0:
		while i < 3:
			aleatorio = lista[random.randrange(0, len(lista) - 0)]

			bot1.append(aleatorio)
			i += 1

		while j < 2:
			aleatorio = lista[random.randrange(0, len(lista) - 0)]

			bot2.append(aleatorio)
			j += 1

	return render(request, 'ver_teste.html', {'teste': t, 'bot1': bot1, 'bot2': bot2})


@login_required(login_url='/accounts/login/')
def fazer_teste(request, teste):
	t = Teste.objects.get(slug=teste)
	imagens = ImagensTeste.objects.filter(dono=t.id)
	imagem = imagens[random.randrange(0, len(imagens) - 0)]
	usuario = request.user
	uid = SocialAccount.objects.get(user_id=usuario.id)

	facebook_uid = uid.uid
	url = "https://graph.facebook.com/v2.5/%s/picture?type=large&redirect=False" % facebook_uid

	avatar_name = "%s.jpg" % facebook_uid

	r = requests.get(url)

	avatar_url = r.json()['data']['url']

	fullfilename = os.path.join('media', avatar_name)
	urllib.urlretrieve(avatar_url, fullfilename)

	back = Image.open(imagem.imagem)
	colagem = Image.open('media/'+str(uid.uid)+'.jpg')

	back.paste(colagem, (111, 125))

	nome_colagem = str(uid.uid)+str(usuario.id)+str(imagem.id)

	croppedname = os.path.join('media', 'cropped'+nome_colagem+'.jpg')
	back.save(croppedname)

	return render(request, 'fazer_teste.html', {'nome_colagem': nome_colagem})