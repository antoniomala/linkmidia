from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from .models import *
from .forms import *
from django.contrib.auth.decorators import login_required


def criar_teste(request):
    form = TesteForm(request.POST or None, request.FILES or None)
    print request.POST
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/administracao/testes')

    return render(request, 'criar_teste.html', {'form': form})


def editar_teste(request, id):
    instancia = Teste.objects.get(pk=id)

    form = TesteForm(request.POST or None, request.FILES or None, instance=instancia)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/administracao/testes')

    return render(request, 'editar_teste.html', {'form': form, 'instancia': instancia})


def excluir_teste(request, id):
    teste = Teste.objects.get(pk=id)
    teste.delete()
    return HttpResponseRedirect('/administracao/testes')


def carregar_imagem_teste(request, id):
    teste = Teste.objects.get(pk=id)
    imagens = ImagensTeste.objects.filter(dono=id)

    form = ImagensTesteForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form = form.save(commit=False)
        form.dono = teste
        form.save()
        
        return HttpResponseRedirect('/carregar_imagem_teste/'+str(teste.id)+'/')

    return render(request, 'carregar_imagem_teste.html', {'form': form, 'imagens': imagens})

def visualizar_imagem_teste(request, teste, id):
    imagem = ImagensTeste.objects.get(pk=id)
    teste = Teste.objects.get(pk=teste)
    return render(request, 'visualizar_imagem_teste.html', {'imagem': imagem})

def excluir_imagem_teste(request, teste, id):
    imagem = ImagensTeste.objects.get(pk=id)
    teste = Teste.objects.get(pk=teste)
    imagem.delete()
    return HttpResponseRedirect('/carregar_imagem_teste/'+str(teste.id)+'/')