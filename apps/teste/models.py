from django.db import models
from django.conf import settings
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.template.defaultfilters import slugify


class Teste(models.Model):
    titulo = models.CharField(u'titulo', max_length=50)
    categoria = models.CharField(u'categoria', max_length=50,
        choices = ((u'Feminino', u'Feminino'), (u'Masculino', u'Masculino'),(u'todos', u'Todos'),), default='todos')
    capa = ProcessedImageField(upload_to='testes', processors=[ResizeToFill(592, 318)], format='JPEG', options={'quality': 80})

    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    slug = models.SlugField(max_length=400, blank=True,
                            null=True, editable=False)

    def save(self, *args, **kwargs):
        super(Teste, self).save(*args, **kwargs)
        slug = "%s-%s" % (slugify(self.titulo), self.id)
        if not self.slug or self.slug != slug:
            self.slug = slug
            self.save()


class ImagensTeste(models.Model):
    dono = models.ForeignKey(Teste, blank=True, null=True)
    titulo = models.CharField(u'Titulo', max_length=50, blank=True, null=True)
    imagem = ProcessedImageField(upload_to='testes', processors=[ResizeToFill(801, 485)], format='JPEG', options={'quality': 80})

    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
