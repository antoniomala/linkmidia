# -*- coding: utf-8 -*-
from django.forms import ModelForm
from .models import *


class TesteForm(ModelForm):
    class Meta:
        model = Teste
        exclude = ['created_on', 'updated_on', 'slug']


class ImagensTesteForm(ModelForm):
    class Meta:
        model = ImagensTeste
        exclude = ['dono', 'created_on', 'updated_on']
